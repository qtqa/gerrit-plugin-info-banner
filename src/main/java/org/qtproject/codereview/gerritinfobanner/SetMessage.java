//
// Copyright (C) 2022-24 The Qt Company
//

package org.qtproject.codereview.gerritinfobanner;

import com.google.gerrit.extensions.annotations.PluginName;
import com.google.gerrit.extensions.api.access.PluginPermission;
import com.google.gerrit.extensions.restapi.AuthException;
import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestModifyView;
import com.google.gerrit.server.config.ConfigResource;
import com.google.gerrit.server.permissions.GlobalPermission;
import com.google.gerrit.server.permissions.PermissionBackend;
import com.google.gerrit.server.permissions.PermissionBackendException;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class SetMessage implements RestModifyView<ConfigResource, BannerSettingInfo> {

  @Inject private PermissionBackend permissionBackend;

  @Inject private MessageStore messageStore;

  @Inject @PluginName String pluginName;

  @Override
  public Response<BannerSettingInfo> apply(ConfigResource rsrc, BannerSettingInfo input)
      throws PermissionBackendException, AuthException {

    if (!permissionBackend
        .currentUser()
        .testOrFalse(
            new PluginPermission(pluginName, SetMessageCapability.SET_BANNER_MESSAGE))) {
      throw new AuthException("not permitted");
    }

    messageStore.setMessage(input.message);

    BannerSettingInfo info = new BannerSettingInfo();
    info.message = messageStore.getMessage();
    info.editable = true;

    return Response.ok(info);
  }
}
