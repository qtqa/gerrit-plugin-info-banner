//
// Copyright (C) 2022-24 The Qt Company
//

package org.qtproject.codereview.gerritinfobanner;

import com.google.inject.Singleton;
import org.apache.commons.text.StringEscapeUtils;

@Singleton
public class MessageStore {

  private static String message = "";

  public String getMessage() {
    return message;
  }

  public void setMessage(String msg) {
    message = StringEscapeUtils.escapeHtml4(msg);
  }
}
