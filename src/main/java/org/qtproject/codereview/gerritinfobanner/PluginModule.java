//
// Copyright (C) 2022-24 The Qt Company
//

package org.qtproject.codereview.gerritinfobanner;

import static com.google.gerrit.server.config.ConfigResource.CONFIG_KIND;
import static org.qtproject.codereview.gerritinfobanner.SetMessageCapability.SET_BANNER_MESSAGE;

import com.google.gerrit.extensions.annotations.Exports;
import com.google.gerrit.extensions.config.CapabilityDefinition;
import com.google.gerrit.extensions.config.FactoryModule;
import com.google.gerrit.extensions.restapi.RestApiModule;

public class PluginModule extends FactoryModule {

  @Override
  protected void configure() {
    bind(CapabilityDefinition.class)
        .annotatedWith(Exports.named(SET_BANNER_MESSAGE))
        .to(SetMessageCapability.class);
    install(
        new RestApiModule() {
          @Override
          protected void configure() {
            get(CONFIG_KIND, "message").to(GetMessage.class);
            post(CONFIG_KIND, "message").to(SetMessage.class);
          }
        });
  }
}
