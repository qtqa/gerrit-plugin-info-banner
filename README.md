# General

    With this plugin administrators can set a banner info message from
    their settings screen. Message is displayed for all users on top of
    the screen.

# Build

    bazelisk build plugins/gerrit-plugin-info-banner

# Test

    bazelisk test --test_output=streamed "//plugins/gerrit-plugin-info-banner:*"
