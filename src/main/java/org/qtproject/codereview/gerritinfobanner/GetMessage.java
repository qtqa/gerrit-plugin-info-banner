//
// Copyright (C) 2022 The Qt Company
//

package org.qtproject.codereview.gerritinfobanner;

import com.google.gerrit.extensions.restapi.Response;
import com.google.gerrit.extensions.restapi.RestReadView;
import com.google.gerrit.server.config.ConfigResource;
import com.google.gerrit.server.permissions.GlobalPermission;
import com.google.gerrit.server.permissions.PermissionBackend;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class GetMessage implements RestReadView<ConfigResource> {

  @Inject private PermissionBackend permissionBackend;

  @Inject private MessageStore messageStore;

  @Override
  public Response<BannerSettingInfo> apply(ConfigResource rsrc) {
    BannerSettingInfo info = new BannerSettingInfo();
    info.message = messageStore.getMessage();
    if (permissionBackend.currentUser().testOrFalse(GlobalPermission.ADMINISTRATE_SERVER)) {
      info.editable = true;
    } else {
      info.editable = false;
    }

    return Response.ok(info);
  }
}
